/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package org.minifx.workbench.conf.fullyconfigured;

import org.minifx.workbench.domain.AbstractFxBorderPaneView;

public class UnannotatedView extends AbstractFxBorderPaneView {
    /* marker */
}
