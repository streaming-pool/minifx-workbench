/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package org.minifx.workbench.domain;

import javafx.scene.layout.BorderPane;

public class AbstractFxBorderPaneView extends BorderPane implements WorkbenchView {
    /* Inherit from here to get it autodetected */
}
