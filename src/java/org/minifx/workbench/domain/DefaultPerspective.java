/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package org.minifx.workbench.domain;

import org.minifx.workbench.annotations.Name;

@Name("Default perspective")
public interface DefaultPerspective extends Perspective {
    /* Default perspective */
}
